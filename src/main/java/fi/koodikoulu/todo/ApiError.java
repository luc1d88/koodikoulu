package fi.koodikoulu.todo;

import java.time.LocalDateTime;
import java.util.List;

public class ApiError {

  private int status;
  private LocalDateTime timestamp;
  private String message;
  private List<String> errors;

  private ApiError() {
    timestamp = LocalDateTime.now();
  }

  ApiError(int status) {
    this();
    this.status = status;
  }

  ApiError(int status, String message) {
    this();
    this.status = status;
    this.message = message;
  }

  ApiError(int status, String message, List<String> errors) {
    this();
    this.status = status;
    this.message = message;
    this.errors = errors;
  }

  /**
   * @return the status
   */
  public int getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(int status) {
    this.status = status;
  }

  /**
   * @return the timestamp
   */
  public LocalDateTime getTimestamp() {
    return timestamp;
  }

  /**
   * @param timestamp the timestamp to set
   */
  public void setTimestamp(LocalDateTime timestamp) {
    this.timestamp = timestamp;
  }

  /**
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * @param message the message to set
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * @return the errors
   */
  public List<String> getErrors() {
    return errors;
  }

  /**
   * @param errors the errors to set
   */
  public void setErrors(List<String> errors) {
    this.errors = errors;
  }
}
