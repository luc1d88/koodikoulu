package fi.koodikoulu.todo.controller.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;

@SuppressWarnings("serial")
public class TodoRequestDTO implements Serializable {
  
  @NotBlank(message = "Name is mandatory")
  private String name;
  
  @NotBlank(message = "Status is mandatory")
  private String status;

  @NotBlank(message = "Status is mandatory")
  private String priority;

  private String description;
  private Date dueDate;

  public TodoRequestDTO() {}

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * @return the priority
   */
  public String getPriority() {
    return priority;
  }

  /**
   * @param priority the priority to set
   */
  public void setPriority(String priority) {
    this.priority = priority;
  }

  /**
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description the description to set
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @return the dueDate
   */
  public Date getDueDate() {
    return dueDate;
  }

  /**
   * @param dueDate the dueDate to set
   */
  public void setDueDate(Date dueDate) {
    this.dueDate = dueDate;
  }
  
}
