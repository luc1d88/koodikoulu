package fi.koodikoulu.todo.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fi.koodikoulu.todo.controller.dto.TodoRequestDTO;
import fi.koodikoulu.todo.controller.dto.TodoResponseDTO;
import fi.koodikoulu.todo.repositories.domain.Todo;
import fi.koodikoulu.todo.service.TodoService;

@RestController
@RequestMapping("/api/todos")
public class TodoController {

  @Autowired
  private ModelMapper modelMapper;

  @Autowired
  private TodoService todoService;

  @GetMapping
  ResponseEntity<List<TodoResponseDTO>> getTodos() {
    List<TodoResponseDTO> todos = todoService.getAllTodos()
      .stream()
      .map(todo -> convertToDto(todo))
      .collect(Collectors.toList());

    return new ResponseEntity<List<TodoResponseDTO>>(todos, HttpStatus.OK);
  }

  @PostMapping()
  ResponseEntity<Boolean> addTodo(@Valid @RequestBody TodoRequestDTO todoDTO) {
    todoService.creatTodo(convertToEntity(todoDTO));
    
    return new ResponseEntity<Boolean>(true, HttpStatus.CREATED);
  }

  @GetMapping("/{id}")
  ResponseEntity<TodoResponseDTO> getTodo(@PathVariable("id") Long id) {
    Todo todo = todoService.getTodoById(id);
    return new ResponseEntity<TodoResponseDTO>(convertToDto(todo), HttpStatus.OK);
  }

  @DeleteMapping("/{id}")
  ResponseEntity<TodoResponseDTO> deleteTodo(@PathVariable("id") Integer id) {
    
    return null;
  }

  @PutMapping("/{id}")
  ResponseEntity<TodoResponseDTO> updateTodo(@Valid @RequestBody TodoRequestDTO todoDto, @PathVariable("id") Integer id) {
    
    return null;
  }

  private Todo convertToEntity(TodoRequestDTO todoDTO) throws ValidationException {
    Todo todo = modelMapper.map(todoDTO, Todo.class);

    return todo;
  }

  private TodoResponseDTO convertToDto(Todo todo) throws ValidationException {
    TodoResponseDTO todoDTO = modelMapper.map(todo, TodoResponseDTO.class);

    return todoDTO;
  }

}