package fi.koodikoulu.todo;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;

import org.modelmapper.MappingException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import fi.koodikoulu.todo.service.ServiceException;

@RestControllerAdvice
public class GlobalExceptionHandlerController extends ResponseEntityExceptionHandler {
  
  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, 
    HttpHeaders headers,
    HttpStatus status, 
    WebRequest request) {
      List<String> errors = new ArrayList<String>();
      for (FieldError error : ex.getBindingResult().getFieldErrors()) {
          errors.add(error.getField() + ": " + error.getDefaultMessage());
      }
      for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
          errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
      }
      return ResponseEntity
        .badRequest()
        .body(new ApiError(HttpStatus.BAD_REQUEST.value(), "Bad Request", errors));
  }

  @Override
  protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex,
    HttpHeaders headers,
    HttpStatus status, 
    WebRequest request) {
      String name = ex.getLocalizedMessage();
      List<String> errors = new ArrayList<String>();
      errors.add(name);
    return ResponseEntity
      .badRequest()
      .body(new ApiError(HttpStatus.BAD_REQUEST.value(), "Bad request", errors));
  }

  @ExceptionHandler({ValidationException.class})
  private ResponseEntity<ApiError> handleValidation(ValidationException ex) {
    
    return ResponseEntity
      .badRequest()
      .body(new ApiError(HttpStatus.BAD_REQUEST.value(), "Bad request"));
  }

  @ExceptionHandler({ServiceException.NotFoundException.class})
  private ResponseEntity<ApiError> handleNotFound(ServiceException.NotFoundException ex) {
    
    return ResponseEntity.status(HttpStatus.NOT_FOUND)
    .body(new ApiError(HttpStatus.NOT_FOUND.value(), "Not Found"));
  }

  @ExceptionHandler({DataIntegrityViolationException.class})
  private ResponseEntity<ApiError> handleDataIntegrityViolation(DataIntegrityViolationException ex) {
    String name = ex.getLocalizedMessage();
    List<String> errors = new ArrayList<String>();
    errors.add(name);
    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
    .body(new ApiError(HttpStatus.BAD_REQUEST.value(), "Missing required attributes", errors));
  }
  
}


