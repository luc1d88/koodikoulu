package fi.koodikoulu.todo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import fi.koodikoulu.todo.repositories.TodoRepository;
import fi.koodikoulu.todo.repositories.domain.Todo;

@Service
public class TodoServiceImpl implements TodoService {

  private final TodoRepository todoRepository;

  public TodoServiceImpl(TodoRepository todoRepository) {
    this.todoRepository = todoRepository;
  }

  @Override
  public void creatTodo(Todo todo) {
    todoRepository.save(todo);
  }

  @Override
  public void deleteTodo(Todo todo) {

  }

  @Override
  public List<Todo> getAllTodos() {
    return todoRepository.getAll();
  }

  @Override
  public Todo getTodoById(Long id) {
    Optional<Todo> todoOptional = todoRepository.getById(id);
    if(!todoOptional.isPresent())
      throw new ServiceException.NotFoundException("Todo not found");
    Todo todo = todoOptional.get();
    return todo; 
  }

  @Override
  public void updateTodo(Todo todo) {
  }

}