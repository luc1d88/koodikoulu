package fi.koodikoulu.todo.service;

import java.util.List;

import fi.koodikoulu.todo.repositories.domain.Todo;

public interface TodoService {
  Todo getTodoById(Long id);
  List<Todo> getAllTodos();
  void creatTodo(Todo todo);
  void updateTodo(Todo todo);
  void deleteTodo(Todo todo);
}