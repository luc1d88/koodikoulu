package fi.koodikoulu.todo.service;

@SuppressWarnings("serial")
public class ServiceException extends RuntimeException {

  public ServiceException(String message) {
    super(message);
  }

  public ServiceException(String message, Throwable cause) {
    super(message, cause);
  }

  public static class NotFoundException extends ServiceException {
    public NotFoundException(String message) {
      super(message);
    }
  }

  public static class AlreadyExistsException extends ServiceException {
    public AlreadyExistsException(String message) {
      super(message);
    }
  }
}
