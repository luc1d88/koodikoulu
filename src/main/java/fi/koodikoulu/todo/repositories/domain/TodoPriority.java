package fi.koodikoulu.todo.repositories.domain;

public enum TodoPriority {
  LOW,
  NORMAL,
  HIGH
}