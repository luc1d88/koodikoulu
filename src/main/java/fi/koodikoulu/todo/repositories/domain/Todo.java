package fi.koodikoulu.todo.repositories.domain;

import java.util.Date;

public class Todo {
  private Long id;
  private String name;
  private String description;
  private TodoStatus status;
  private TodoPriority priority;
  private Date created;
  private Date dueDate;

  public Todo() {
    this.created = new Date();
  }

  public Todo(Long id, String name, String description, TodoStatus status, TodoPriority priority) {
    this();
    this.id = id;
    this.name = name;
    this.description = description;
    this.status = status;
  }

  public Todo(Long id, String name, String description, TodoStatus status, TodoPriority priority, Date dueDate) {
    this();
    this.id = id;
    this.name = name;
    this.description = description;
    this.status = status;
    this.dueDate = dueDate;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the created
   */
  public Date getCreated() {
    return created;
  }

  /**
   * @param created the created to set
   */
  public void setCreated(Date created) {
    this.created = created;
  }

  /**
   * @return the status
   */
  public TodoStatus getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(TodoStatus status) {
    this.status = status;
  }

  /**
   * @return the priority
   */
  public TodoPriority getPriority() {
    return priority;
  }

  /**
   * @param priority the priority to set
   */
  public void setPriority(TodoPriority priority) {
    this.priority = priority;
  }

  /**
   * @return the dueDate
   */
  public Date getDueDate() {
    return dueDate;
  }

  /**
   * @param dueDate the dueDate to set
   */
  public void setDueDate(Date dueDate) {
    this.dueDate = dueDate;
  }

  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description the description to set
   */
  public void setDescription(String description) {
    this.description = description;
  }
  
  
}