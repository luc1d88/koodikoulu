package fi.koodikoulu.todo.repositories.domain;

public enum TodoStatus {
  ON_HOLD,
  IN_PROGRESS,
  READY_TO_START,
  DONE
}