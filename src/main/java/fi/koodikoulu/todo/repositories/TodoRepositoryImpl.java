package fi.koodikoulu.todo.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import fi.koodikoulu.todo.repositories.domain.Todo;

@Repository
public class TodoRepositoryImpl implements TodoRepository {
  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Autowired
  private TodoRowMapper todoRowMapper;

  private final String FIND_BY_ID_QUERY = 
    "SELECT * " +
    "FROM TODOS " +
    "WHERE id = ?";

  private final String FIND_ALL_QUERY = 
    "SELECT * " +
    "FROM TODOS ";
  
  private final String UPDATE_QUERY = 
    "UPDATE TODOS " + 
    "SET name = ? " + 
    "SET description = ? " + 
    "SET status = ? " + 
    "SET priority = ? " + 
    "SET due_date = ? " + 
    "WHERE id = ?";

  private final String INSERT_QUERY = 
    "INSERT INTO TODOS " +
    "(name, description, status, priority, created, due_date) " +
    "VALUES (?, ?, ?, ?, ?, ?)";

  private final String DELETE_QUERY = "";

  @Override
  public int delete(Todo todo) {
    return 0;
  }

  @Override
  public List<Todo> getAll() {
    return jdbcTemplate.query(FIND_ALL_QUERY, todoRowMapper);
  }

  @Override
  public Optional<Todo> getById(Long id) {
    try {
      Todo item = jdbcTemplate.queryForObject(FIND_BY_ID_QUERY, new Object[]{id}, todoRowMapper);
      return Optional.of(item);
    } catch (EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }

  @Override
  public int save(Todo todo) {
    return jdbcTemplate.update(INSERT_QUERY, todo.getName(), todo.getDescription(), todo.getStatus().name(), todo.getPriority().name(), todo.getCreated(), todo.getDueDate());   
  }

  @Override
  public int update(Todo todo) {
    return jdbcTemplate.update(UPDATE_QUERY, todo.getName(), todo.getDescription(), todo.getStatus().name(), todo.getPriority().name(), todo.getDueDate());   
  }
  
}