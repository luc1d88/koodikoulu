package fi.koodikoulu.todo.repositories;

import java.util.List;
import java.util.Optional;

import fi.koodikoulu.todo.repositories.domain.Todo;

public interface TodoRepository {
  Optional<Todo> getById(Long id);
  List<Todo> getAll();
  int save(Todo todo);
  int update(Todo todo);
  int delete(Todo todo);
}