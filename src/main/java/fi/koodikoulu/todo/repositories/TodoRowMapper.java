package fi.koodikoulu.todo.repositories;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import fi.koodikoulu.todo.repositories.domain.Todo;
import fi.koodikoulu.todo.repositories.domain.TodoPriority;
import fi.koodikoulu.todo.repositories.domain.TodoStatus;

@Component
public class TodoRowMapper implements RowMapper<Todo> {
  
  @Override
  public Todo mapRow(ResultSet rs, int rowNum) throws SQLException {
    Todo todo = new Todo();
    todo.setId(rs.getLong("id"));
    todo.setName(rs.getString("name"));
    todo.setDescription(rs.getString("description"));
    todo.setStatus(TodoStatus.valueOf(rs.getString("status")));
    todo.setPriority(TodoPriority.valueOf(rs.getString("priority")));
    todo.setCreated(rs.getDate("created"));
    todo.setDueDate(rs.getDate("due_date"));

    return todo;
  }
}