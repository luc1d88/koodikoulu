
package fi.koodikoulu.todo.security;

import com.auth0.spring.security.api.JwtWebSecurityConfigurer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
@Configuration
public class JwtSecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Value("${auth0.audience}")
  private String audience;

  @Value("${auth0.issuer}")
  private String issuer;

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
      JwtWebSecurityConfigurer
                .forRS256(audience, issuer)
                .configure(httpSecurity)
                .authorizeRequests()
                .antMatchers("/api/**").permitAll()
                .antMatchers("/h2-console/**").permitAll();

                httpSecurity.csrf().disable();
                httpSecurity.headers().frameOptions().disable();
    }
}
